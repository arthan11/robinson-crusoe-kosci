function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomDices() {
  var box = document.querySelector('.container').children[0],
      panelClassName = 'show-front';

  var side = getRandomInt(1, 6);

  box.removeClassName( panelClassName );
  switch (side) {
	case 1: 
	  panelClassName = 'show-front';
	  break;
	case 2: 
	  panelClassName = 'show-back';
	  break;
	case 3: 
	  panelClassName = 'show-right';
	  break;
	case 4: 
	  panelClassName = 'show-left';
	  break;
	case 5: 
	  panelClassName = 'show-top';
	  break;
	case 6: 
	  panelClassName = 'show-bottom';
	  break;	  
  };
  box.addClassName( panelClassName );

  if ( $('#container2').length ) {
    var box2 = document.querySelector('#container2').children[0],
      panelClassName2 = 'show-front';

    var side2 = getRandomInt(1, 6);

	box2.removeClassName( panelClassName );
	  switch (side2) {
		case 1: 
		  panelClassName2 = 'show-front';
		  break;
		case 2: 
		  panelClassName2 = 'show-back';
		  break;
		case 3: 
		  panelClassName2 = 'show-right';
		  break;
		case 4: 
		  panelClassName2 = 'show-left';
		  break;
		case 5: 
		  panelClassName2 = 'show-top';
		  break;
		case 6: 
		  panelClassName2 = 'show-bottom';
		  break;	  
	  };
	  box2.addClassName( panelClassName2 );
  };
	  
  if ( $('#container3').length ) {
    var box3 = document.querySelector('#container3').children[0],
      panelClassName3 = 'show-front';

    var side3 = getRandomInt(1, 6);

	box3.removeClassName( panelClassName3 );
	  switch (side3) {
		case 1: 
		  panelClassName3 = 'show-front';
		  break;
		case 2: 
		  panelClassName3 = 'show-back';
		  break;
		case 3: 
		  panelClassName3 = 'show-right';
		  break;
		case 4: 
		  panelClassName3 = 'show-left';
		  break;
		case 5: 
		  panelClassName3 = 'show-top';
		  break;
		case 6: 
		  panelClassName3 = 'show-bottom';
		  break;	  
	  };
	  box3.addClassName( panelClassName3 );
	  
  };
  
    document.body.style.backgroundColor = "red";
	$("body").animate({ backgroundColor: "white" });
};

var init = function() {
	var shakeEvent = new Shake({threshold: 15});
	shakeEvent.start();
	window.addEventListener('shake', function(){
		randomDices();
		//alert("Shaked");
	}, false);

	$( "body" ).click(function() {
	  randomDices();
	});
	
  randomDices();
};

  
window.addEventListener( 'DOMContentLoaded', init, false);